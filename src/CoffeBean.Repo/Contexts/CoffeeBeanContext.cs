﻿using System.Data.Entity;
using CoffeeBean.Dom;

namespace CoffeeBean.Repo
{
    public class CoffeeBeanContext : DbContext
    {
        public DbSet<Missie> Missies { get; set; }
        public DbSet<Werknemer> Werknemers { get; set; }
    }
}
