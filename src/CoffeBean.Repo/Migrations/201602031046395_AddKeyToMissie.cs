namespace CoffeeBean.Repo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddKeyToMissie : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Missies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MissieNaam = c.String(),
                        BeginDatum = c.DateTime(nullable: false),
                        EindDatum = c.DateTime(nullable: false),
                        TariefPerUur = c.Double(nullable: false),
                        KansOpVerleging = c.Int(nullable: false),
                        KlantNaam = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Missies");
        }
    }
}
