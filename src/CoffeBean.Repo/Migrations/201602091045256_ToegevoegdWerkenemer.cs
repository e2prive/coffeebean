namespace CoffeeBean.Repo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ToegevoegdWerkenemer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Werknemers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Voornaam = c.String(),
                        Naam = c.String(),
                        Aandachtspunt = c.String(),
                        InDiestDatum = c.DateTime(nullable: false),
                        EersteErvaring = c.DateTime(nullable: false),
                        DagTarief = c.Double(),
                        UurTarief = c.Double(),
                        UurTarief1 = c.Double(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Werknemers");
        }
    }
}
