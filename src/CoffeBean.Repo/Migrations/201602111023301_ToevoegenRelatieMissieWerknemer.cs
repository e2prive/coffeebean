namespace CoffeeBean.Repo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ToevoegenRelatieMissieWerknemer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Werknemers", "Missie_Id", c => c.Int());
            CreateIndex("dbo.Werknemers", "Missie_Id");
            AddForeignKey("dbo.Werknemers", "Missie_Id", "dbo.Missies", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Werknemers", "Missie_Id", "dbo.Missies");
            DropIndex("dbo.Werknemers", new[] { "Missie_Id" });
            DropColumn("dbo.Werknemers", "Missie_Id");
        }
    }
}
