namespace CoffeeBean.Repo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveWerknemerfromMissie : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Werknemers", "Missie_Id", "dbo.Missies");
            DropIndex("dbo.Werknemers", new[] { "Missie_Id" });
            DropColumn("dbo.Werknemers", "Missie_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Werknemers", "Missie_Id", c => c.Int());
            CreateIndex("dbo.Werknemers", "Missie_Id");
            AddForeignKey("dbo.Werknemers", "Missie_Id", "dbo.Missies", "Id");
        }
    }
}
