using System.IO;
using CoffeeBean.Dom;
using System;
using System.Data.Entity.Migrations;
using System.Globalization;

namespace CoffeeBean.Repo.Migrations
{

    internal sealed class Configuration : DbMigrationsConfiguration<CoffeeBeanContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CoffeeBeanContext context)
        {
            //AddTestWerknemers(context);
            //AddTestMissies(context);
        }

        private static void AddTestMissies(CoffeeBeanContext context)
        {
            string resourceName = "missie_seed.csv";
            using (StreamReader reader = new StreamReader("C:\\Users\\houbesj\\Documents\\coffeebean\\doc\\" + resourceName))
            {
                while (!reader.EndOfStream)
                {
                    var splits = reader.ReadLine().Split(';');
                    Missie missie = new Missie();

                    missie.MissieNaam = splits[0];
                    missie.BeginDatum = Convert.ToDateTime(splits[1]);
                    missie.EindDatum = Convert.ToDateTime(splits[2]);
                    missie.TariefPerUur = double.Parse(splits[3], CultureInfo.InvariantCulture);

                    Random random = new Random();
                    missie.KansOpVerleging = (random.Next(0, 100));

                    missie.KlantNaam = splits[5];
                    context.Missies.AddOrUpdate(missie);
                }
            }
        }

        private static
            void AddTestWerknemers(CoffeeBeanContext context)
        {
            string resourceName = "werknemer_seed.csv";
            using (StreamReader reader = new StreamReader("C:\\Users\\houbesj\\Documents\\coffeebean\\doc\\" + resourceName))
            {
                while (!reader.EndOfStream)
                {
                    var splits = reader.ReadLine().Split(';');

                    Random random = new Random();
                    int randomInteger = random.Next(1, 3);

                    Werknemer werknemer = new Werknemer();

                    if (randomInteger == 1)
                    {
                        werknemer = new Consultant();
                        ((Consultant) werknemer).DagTarief = double.Parse(splits[5], CultureInfo.InvariantCulture);
                    }
                    else if (randomInteger == 2)
                    {
                        werknemer = new Zelfstandige();
                        ((Zelfstandige)werknemer).UurTarief = double.Parse(splits[5], CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        werknemer = new Freelancer();
                        ((Freelancer)werknemer).UurTarief = double.Parse(splits[5], CultureInfo.InvariantCulture);
                    }

                    werknemer.Voornaam = splits[0];
                    werknemer.Naam = splits[1];
                    werknemer.InDiestDatum = Convert.ToDateTime(splits[3]);
                    werknemer.EersteErvaring = Convert.ToDateTime(splits[4]);
                    context.Werknemers.AddOrUpdate(werknemer);
                }
            }
        }
    }
}
