﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoffeeBean.Dom
{
    public class Missie
    {
        [Key]
        public int Id { get; set; }
        public string MissieNaam { get; set; }
        public DateTime BeginDatum { get; set; }
        public DateTime EindDatum { get; set; }
        public double TariefPerUur { get; set; }
        public int KansOpVerleging { get; set; }
        public string KlantNaam { get; set; }
        //public ICollection<Werknemer> Werknemers { get; set; }
    }
}
