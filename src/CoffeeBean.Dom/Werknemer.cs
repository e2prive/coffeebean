﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoffeeBean.Dom
{
    public class Werknemer
    {
        [Key]
        public int Id { get; set; }
        public string Voornaam { get; set; }
        public string Naam { get; set; }
        public string Aandachtspunt { get; set; }
        public DateTime InDiestDatum { get; set; }
        public DateTime EersteErvaring { get; set; }
    }
}
