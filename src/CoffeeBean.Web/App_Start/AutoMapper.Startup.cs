﻿using AutoMapper;
using CoffeeBean.Web.Configuration.Mappings;

namespace CoffeeBean.Web
{
    public partial class Startup
    {
        public void ConfigureAutomapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<MissieMappingProfile>();
                cfg.AddProfile<WerknemerMappingProfile>();
            });
        }
    }
}