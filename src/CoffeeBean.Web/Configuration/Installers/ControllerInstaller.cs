﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using CoffeeBean.Web.Controllers;
using System.Web.Mvc;

namespace CoffeeBean.Web.Configuration.Installers
{
    public class ControllerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<MissieController>().BasedOn<IController>().LifestylePerWebRequest());
        }
    }
}