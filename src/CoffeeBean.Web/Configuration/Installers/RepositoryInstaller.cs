﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using CoffeeBean.Repo;
using CoffeeBean.Repo.Interfaces;
using System.Data.Entity;

namespace CoffeeBean.Web.Configuration.Installers
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<DbContext>()
                    .ImplementedBy<CoffeeBeanContext>()
                    .LifeStyle.PerWebRequest
            );

            container.Register(
                Component.For(typeof(IRepository<>))
                    .ImplementedBy(typeof(RepositoryBase<>))
                    .LifeStyle.PerWebRequest
            );
        }
    }
}