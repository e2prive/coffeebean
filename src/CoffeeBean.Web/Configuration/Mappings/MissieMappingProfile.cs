﻿using AutoMapper;
using CoffeeBean.Dom;
using CoffeeBean.Web.Models;


namespace CoffeeBean.Web.Configuration.Mappings
{
    public class MissieMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<MissieModel, Missie>().ReverseMap();
            Mapper.AssertConfigurationIsValid();
        }
    }
}