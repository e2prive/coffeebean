﻿using AutoMapper;
using CoffeeBean.Dom;
using CoffeeBean.Web.Models;


namespace CoffeeBean.Web.Configuration.Mappings
{
    public class WerknemerMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<WerknemerModel, Werknemer>().ReverseMap();
            Mapper.AssertConfigurationIsValid();
        }
    }
}