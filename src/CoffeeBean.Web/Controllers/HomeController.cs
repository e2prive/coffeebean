﻿using System.Web.Mvc;

namespace CoffeeBean.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}