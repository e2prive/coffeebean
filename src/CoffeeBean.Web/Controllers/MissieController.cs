﻿using CoffeeBean.Dom;
using CoffeeBean.Web.ViewModel;
using CoffeeBean.Repo;
using CoffeeBean.Repo.Interfaces;
using CoffeeBean.Web.Extensions;
using CoffeeBean.Web.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Castle.Core.Logging;

namespace CoffeeBean.Web.Controllers
{
    public class MissieController : Controller
    {
        public IRepository<Missie> MissieRepository { get; set; }

        // GET: Missie
        public ActionResult Index()
        {
            var Missies = MissieRepository.GetAll();
            var model = new MissieIndexViewModel();
            model.Missies = Missies.ToModel<MissieModel>().ToList();
            //model.Missies.ToList().ForEach(f => instantiateNullObjects(f)); --> later nodig voor consultant == null

            return View(model);
        }

        [HttpGet]
        public ActionResult Missie(int id)
        {
            var resultCb = new Missie();
            resultCb = MissieRepository.GetFirstOrDefault(f => f.Id == id);
            var modelCb = resultCb.ToModel<MissieModel>();

            var model = new MissieViewModel
            {
                Missie = modelCb
            };

            return View("MissieForm",model);
        }

        [HttpPost]
        public ActionResult Missie(MissieViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            UpdateModel(model);
            return RedirectToAction("Missie", new { id = model.Missie.Id });
        }

        [HttpGet]
        public ActionResult CreateMissie()
        {
            var viewModel = new MissieViewModel
            {
                Missie = new MissieModel { }
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CreateMissie(MissieViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var createMissieId = CreateNewMissie(model);

            return RedirectToAction("Missie", new { id = createMissieId });
        }

        private int CreateNewMissie(MissieViewModel model)
        {
            var missie = model.Missie.ToModel<Missie>();
            ApplyChanges(model, missie);

            var createNewMissie = MissieRepository.Add(missie);
            MissieRepository.SaveChanges();

            return createNewMissie.Id;
        }

        private void ApplyChanges(MissieViewModel model, Missie missie)
        {
            missie.KansOpVerleging = model.Missie.KansOpVerleging;
            missie.KlantNaam = model.Missie.KlantNaam;
            missie.MissieNaam = model.Missie.MissieNaam;
            missie.TariefPerUur = model.Missie.TariefPerUur;
            missie.BeginDatum = model.Missie.BeginDatum;
            missie.EindDatum = model.Missie.EindDatum;
        }

        public void UpdateFormulier(MissieViewModel model)
        {
            var originalMissie = MissieRepository.GetFirstOrDefault(f => f.Id == model.Missie.Id);
            if (originalMissie == null)
            {
                throw new HttpException(404, "Het gevraagde missie werd niet gevonden");
            }

            ApplyChanges(model, originalMissie);
            MissieRepository.Update(originalMissie);
            MissieRepository.SaveChanges();
        }

        [HttpPost]
        public ActionResult UpdateMissie(MissieViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("MissieForm",model);
            }
            UpdateFormulier(model);
            return RedirectToAction("Index");
        }
    }
}