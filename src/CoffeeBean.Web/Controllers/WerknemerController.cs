﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoffeeBean.Dom;
using CoffeeBean.Repo.Interfaces;
using CoffeeBean.Web.Extensions;
using CoffeeBean.Web.Models;
using CoffeeBean.Web.ViewModel;

namespace CoffeeBean.Web.Controllers
{
    public class WerknemerController : Controller
    {
        public IRepository<Werknemer> WerknemersRepository { get; set; }
        // GET: Werknemer
        public ActionResult Index()
        {
            var Werknemers = WerknemersRepository.GetAll();
            var model = new WerknemerIndexViewModel();
            model.Werknemers = Werknemers.ToModel<WerknemerModel>().ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult CreateWerknemer()
        {
            var viewModel = new WerknemerViewModel
            {
                Werknemer = new WerknemerModel { }
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CreateWerknemer(WerknemerViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var createWerknemerId = CreateNewWerknemer(model);

            return RedirectToAction("Werknemer", new { id = createWerknemerId });
        }

        private int CreateNewWerknemer(WerknemerViewModel model)
        {
            var werknemer = model.Werknemer.ToModel<Werknemer>();
            ApplyChanges(model, werknemer);

            var createNewWerknemer = WerknemersRepository.Add(werknemer);
            WerknemersRepository.SaveChanges();

            return createNewWerknemer.Id;
        }

        private void ApplyChanges(WerknemerViewModel model, Werknemer werknemer)
        {
            werknemer.Voornaam = model.Werknemer.Voornaam;
            werknemer.Naam = model.Werknemer.Naam;
            werknemer.Aandachtspunt= model.Werknemer.Aandachtspunt;
            werknemer.InDiestDatum = model.Werknemer.InDiestDatum;
            werknemer.EersteErvaring = model.Werknemer.EersteErvaring;
        }


        [HttpGet]
        public ActionResult Werknemer(int id)
        {
            var resultCb = new Werknemer();
            resultCb = WerknemersRepository.GetFirstOrDefault(f => f.Id == id);
            var modelCb = resultCb.ToModel<WerknemerModel>();

            var model = new WerknemerViewModel
            {
                Werknemer = modelCb
            };

            return View("WerknemerForm", model);
        }

        [HttpPost]
        public ActionResult UpdateWerknemer(WerknemerViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("WerknemerForm", model);
            }
            UpdateFormulier(model);
            return RedirectToAction("Index");
        }

        public void UpdateFormulier(WerknemerViewModel model)
        {
            var originalWerknemer = WerknemersRepository.GetFirstOrDefault(f => f.Id == model.Werknemer.Id);
            if (originalWerknemer == null)
            {
                throw new HttpException(404, "De gevraagde werknemer werd niet gevonden");
            }

            ApplyChanges(model, originalWerknemer);
            WerknemersRepository.Update(originalWerknemer);
            WerknemersRepository.SaveChanges();
        }
    }
}