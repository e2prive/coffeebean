﻿using CoffeeBean.Dom;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CoffeeBean.Web.Models
{
    public class MissieModel
    {
        public int Id { get; set; }

        [Display(Name = "Naam van missie")]
        [Required]
        public string MissieNaam { get; set; }

        [Display(Name = "Start datum")]
        [Required]
        public DateTime BeginDatum { get; set; }

        [Display(Name = "Eind datum")]
        [Required]
        public DateTime EindDatum { get; set; }

        [Display(Name = "Tarief per uur")]
        [Required]
        public double TariefPerUur { get; set; }

        [Display(Name = "Kans op verlenging")]
        [Required]
        public int KansOpVerleging { get; set; }

        [Display(Name = "Klant naam")]
        [Required]
        public string KlantNaam { get; set; }
    }
}