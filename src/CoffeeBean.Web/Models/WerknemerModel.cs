﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoffeeBean.Web.Models
{
    public class WerknemerModel
    {
        public int Id { get; set; }

        [Display(Name = "Voornaam")]
        [Required]
        public string Voornaam { get; set; }

        [Display(Name = "Naam")]
        [Required]
        public string Naam { get; set; }

        [Display(Name = "Aandachtspunt")]
        public string Aandachtspunt { get; set; }

        [Display(Name = "Datum in diensttreding")]
        [Required]
        public DateTime InDiestDatum { get; set; }

        [Display(Name = "Datum eerste ervaring")]
        [Required]
        public DateTime EersteErvaring { get; set; }
    }
}