﻿using CoffeeBean.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoffeeBean.Web.ViewModel
{
    public class MissieIndexViewModel
    {
        public IList<MissieModel> Missies { get; set; }
    }
}