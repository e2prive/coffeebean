﻿using CoffeeBean.Web.Models;
using System.Collections.Generic;

namespace CoffeeBean.Web.ViewModel
{
    public class WerknemerIndexViewModel
    {
        public IList<WerknemerModel> Werknemers { get; set; }
    }
}