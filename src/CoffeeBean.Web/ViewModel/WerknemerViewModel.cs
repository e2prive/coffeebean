﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoffeeBean.Web.Models;

namespace CoffeeBean.Web.ViewModel
{
    public class WerknemerViewModel
    {
        public WerknemerModel Werknemer { get; set; }
    }
}